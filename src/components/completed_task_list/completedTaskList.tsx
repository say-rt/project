import * as React from "react";
import {
    Button,
    Container, Divider,
    Header,
    Segment, Table
} from "semantic-ui-react";
import {Task, service} from "../../Service";
import {v4 as uuid} from 'uuid';
import {IfBox} from "../style/if";


export interface CompletedTaskListProps {
    update: boolean,
}

export interface CompletedTaskListState {
    loading: boolean
    update: boolean
    tasks: Task[]
}



export class CompletedTaskList extends React.Component<CompletedTaskListProps, CompletedTaskListState> {

    constructor(props: any) {
        super(props)
        this.state = {
            loading: false,
            update: false,
            tasks: service.completedTasks
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps: Readonly<CompletedTaskListProps>, prevState: Readonly<CompletedTaskListState>, snapshot?: any) {
        if(this.state.update !== this.props.update){
            // console.log("LIST STATE",this.state)
            this.setState({update: this.props.update,tasks: service.completedTasks})
            this.forceUpdate()
        }
    }

    render() {
        return <Container>

            <Header as='h2' floated='left'>
                Completed Tasks
            </Header>
            <Divider hidden/>
            <Divider hidden/>
            <Divider hidden/>
            <Divider />
            <Segment attached style={{padding: "10px"}} loading={this.state.loading}>
                <Table basic='very' celled collapsing style={{ overflow: "hidden", width:"100%"}}>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Task</Table.HeaderCell>
                            <Table.HeaderCell>Start Time</Table.HeaderCell>
                            <Table.HeaderCell>Exec Time</Table.HeaderCell>
                            <Table.HeaderCell>Comp Time</Table.HeaderCell>
                            {/*<Table.HeaderCell>Time spent</Table.HeaderCell>*/}
                            <Table.HeaderCell>Wait Time</Table.HeaderCell>
                            <IfBox shouldShow={service.algo === "Priority"}>
                                <Table.HeaderCell>Priority</Table.HeaderCell>
                            </IfBox>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.tasks.map( (task,i) =>
                            <Table.Row key={uuid()}>
                                <Table.Cell>{task.name}</Table.Cell>
                                <Table.Cell>{task.startTime}</Table.Cell>
                                <Table.Cell>{task.executionTime}</Table.Cell>
                                <Table.Cell>{task.completionTime}</Table.Cell>
                                {/*<Table.Cell>{task.elapsedTimeOnMe}</Table.Cell>*/}
                                <Table.Cell>{task.waitTime}</Table.Cell>

                                <IfBox shouldShow={service.algo === "Priority"}>
                                    <Table.Cell>{task.priority}</Table.Cell>
                                </IfBox>
                            </Table.Row>)}
                    </Table.Body>
                </Table>
            </Segment>
        </Container>
    }
}