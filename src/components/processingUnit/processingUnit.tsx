import * as React from "react";
import {
    Button, Checkbox,
    Container, Divider, Dropdown,
    Form, FormField,
    Grid,
    Header, Icon, Image,
    Segment,
} from "semantic-ui-react";
import {Task, service, Notification} from "../../Service";
import {v4 as uuid} from "uuid";
import {IfBox} from "../style/if";
import img from "./img/img_1.png"
import img2 from "./img/img2.gif"
export interface ProcessingUnitProps {
    updateList: (notification) => void,
}

export interface ProcessingUnitState {
    name: string,
    timer: number,
    startTime: number,
    executionTime: number,
    loading: boolean
    hotTask: Task,
    processing: boolean
    paused: boolean
    finished: boolean
    stopOnFinish: boolean
}


export class ProcessingUnit extends React.Component<ProcessingUnitProps, ProcessingUnitState> {
    Timer

    constructor(props: any) {
        super(props)
        this.state = {
            name: 'T' + service.tasks.length,
            startTime: service.timeElapsed || 0,
            timer: 0,
            executionTime: 0,
            hotTask: null,
            processing: false,
            paused: false,
            finished: false,
            stopOnFinish: false,
            loading: false,
        }
    }

    componentDidMount() {
    }

    render() {
        // let timeElapsedOnCurrentTask = this.state.timer - this.state.hotTask?.processingStartedOn;
        return <Container>

            <Header as='h5' floated='right'>
                <Button.Group>
                    <Button icon onClick={this.startProcessing} color={"green"}
                            disabled={(this.state.processing && !this.state.paused)}>
                        <Icon name='play'/>
                    </Button>
                    <Button icon onClick={this.pauseProcessing} color={"blue"}
                            disabled={!this.state.processing || this.state.paused}>
                        <Icon name='pause'/>
                    </Button>
                    <Button icon onClick={this.stopProcessing} color={"red"} disabled={!this.state.processing}>
                        <Icon name='stop'/>
                    </Button>
                </Button.Group>
            </Header>
            <Header as='h2' floated='left'>
                Processing Unit
                <IfBox shouldShow={service.processing}>
                    <img src={img2} style={{width: "44px"}}/>
                </IfBox>
                <IfBox shouldShow={!service.processing}>
                    <img src={img} style={{width: "44px"}}/>
                </IfBox>
            </Header>
            <Divider hidden/>
            <Divider hidden/>
            <Divider hidden/>
            <Divider />

            <Segment attached>
                <Form>
                    <Grid>
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Time Elapsed:</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                {this.state.timer > 0 ? this.state.timer : 0}
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Algorithm:</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                <Dropdown placeholder='Select algo' selection
                                          defaultValue={service.algo}
                                          disabled={this.state.processing || this.state.paused}
                                          options={service.algos} onChange={this.onAlgoChange}/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Stop on finish</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                <FormField>
                                    <Checkbox toggle onChange={this.onStopOnFinishChange}/>
                                </FormField>
                            </Grid.Column>
                        </Grid.Row>
                        <hr/>
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Task in Process:</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                {this.state.hotTask?.name}
                            </Grid.Column>
                        </Grid.Row>
                        {(service.algo === 'Priority') ?
                            <Grid.Row columns={2}>
                                <Grid.Column width={6} verticalAlign={"middle"}>
                                    <label>Priority:</label>
                                </Grid.Column>
                                <Grid.Column textAlign={"right"}>
                                    {this.state.hotTask?.priority}
                                </Grid.Column>
                            </Grid.Row>
                            : null}
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Execution Time:</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                {this.state.hotTask?.executionTime}
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Time spent on this Task:</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                {this.state.hotTask?.elapsedTimeOnMe}
                                {/*{timeElapsedOnCurrentTask > 0 ? timeElapsedOnCurrentTask : 0}*/}
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Form>
            </Segment>
        </Container>
    }

    onStopOnFinishChange = (e) => {
        this.setState((prevState) => ({stopOnFinish: !prevState.stopOnFinish}), () => {
            service.stopOnFinish = this.state.stopOnFinish;
        })
    };

    startProcessing = () => {
        const notification: Notification = {
            hasError: false,
            notification: "Processing Started!",
            showNotification: true
        }
        if(service.stoppedOnce){
            service.completedTasks = [];
            service.stoppedOnce = false;
        }
        this.nextTask();
        this.setState({processing: true, paused: false})
        this.props.updateList(notification)
        service.processing = true;
        service.tasks.forEach(task =>
        {
            if(task.recurring){
                service.recurringTimers = []
                const newInterval = setInterval((task) => {
                    console.log(uuid());
                    let newRecurringTask2 = new Task();
                    if(newRecurringTask2 && task){
                        newRecurringTask2.name = task.name + "(r " + ")"
                        newRecurringTask2.startTime = task.startTime > service.timeElapsed ? task.startTime : service.timeElapsed;
                        newRecurringTask2.executionTime = task.executionTime;
                        console.log(service.tasks)
                        service.addTask(newRecurringTask2)
                    }
                }, task.recurringInterval * 1000)
                service.recurringTimers.push(newInterval);

            }
        });
    }

    processing = (task: Task) => {
        this.setState((prevState) => ({timer: prevState.timer + 1}), () => {
            service.timeElapsed++;
            console.log("PROCEEESING:   ",this.state.timer,"service: ", service.timeElapsed)
            if (task) {
                if (!task.inProcessing && task.startTime <= this.state.timer) {
                    task.start(this.state.timer);
                }
                if (task.inProcessing) {
                    // console.log("task.elapsedTimeOnMe   :", task.elapsedTimeOnMe)
                    // console.log("task.executionTime   :", task.executionTime)
                    task.process();
                    if (task.elapsedTimeOnMe >= +task.executionTime) {
                        // clearInterval(this.Timer);
                        task.complete(this.state.timer);
                        this.nextTask()
                        const notification: Notification = {
                            hasError: false,
                            notification: "Task " + task.name + " Completed!",
                            showNotification: true
                        }
                        this.props.updateList(notification)
                    }
                }
            }else{
                this.nextTask()
            }
            this.props.updateList("")
        })

    }

    startInterval = () => {
        if (!this.Timer) {
            this.Timer = setInterval(() => this.processing(this.state.hotTask), 1000)
            this.props.updateList({})
        }

    }

    nextTask = () => {
        let isStopOnFinsih : boolean = false;
        let taskInProcess: Task = undefined
        if (this.state.paused) {
            taskInProcess = this.state.hotTask
        } else {
            if (service.tasks.length === 0) {
                if (service.stopOnFinish) {
                    service.finished = true;
                    this.stopProcessing()
                    isStopOnFinsih = true;
                } else {
                    this.setState({hotTask: null})
                }
            } else {
                taskInProcess = service.algoSelection();
                service.removeTask(taskInProcess.id)
                taskInProcess.processingStartedOn = service.timeElapsed;
            }
        }

        this.setState((prevState) => ({hotTask: taskInProcess}), () => {
          if(!isStopOnFinsih){
              this.startInterval();
          }
        })
    }


    pauseProcessing = () => {
        const notification: Notification = {
            hasError: false,
            notification: "Processing Paused!",
            showNotification: true
        }
        clearInterval(this.Timer);
        this.Timer = null;
        service.recurringTimers.forEach(timer => clearInterval(timer));
        // service.processing = false;
        // if (service.stopOnFinish)
        //     service.finished = true;
        this.setState({paused: true})
        this.props.updateList(notification)
    }

    stopProcessing = () => {
        const notification: Notification = {
            hasError: false,
            notification: "Processing Stopped!",
            showNotification: true
        }
        service.recurringTimers.forEach(timer => clearInterval(timer));
        clearInterval(this.Timer);
        this.Timer = null;

        service.processing = false;
        service.stoppedOnce = true;
        if (service.stopOnFinish) {
            service.finished = true;
        }
        this.setState({hotTask: null, processing: false, paused: false, timer: 0})
        service.timeElapsed = 0;
        this.props.updateList(notification)
    }

    onAlgoChange = (e, {value}) => {
        console.log(value)
        service.algo = value;
        const notification: Notification = {
            hasError: false,
            notification: "Algorithm changed!",
            showNotification: true
        }
        this.props.updateList(notification)
    }
}