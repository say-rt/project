import * as React from "react";
import {Container, Grid, GridColumn, GridRow, Message, Rail, Segment} from "semantic-ui-react";
import {AddTask} from "../add_task/addTask";
import {TaskList} from "../task_list/taskList";
import {ProcessingUnit} from "../processingUnit/processingUnit";
import {CompletedTaskList} from "../completed_task_list/completedTaskList";
import {Notification, service} from "../../Service";

export class Home extends React.Component<any, any> {
    constructor(props: any) {
        super(props)
        this.state = {
            update: true,
            notification: "",
            showNotification: false,
            hasError: false,

        }
    }

    componentDidMount() {
    }

    render() {
        return <Container style={{width: '95%',paddingTop: "10px"}}>

            <Segment style={{height: "98vh", overflow: "auto"}}>
                {this.state.showNotification ?
                    <Rail internal position='right' style={{zIndex: '999'}}>
                        <Message positive={!this.state.hasError} negative={this.state.hasError}>
                            <Message.Header>{this.state.notification}</Message.Header>
                        </Message>
                    </Rail>
                    : null}
                    <Grid stackable columns={3} divided>
                    <GridColumn >
                        <AddTask taskAdded={(notification) => this.update(notification)} startTime={this.state.startTime + 1}/>
                        <TaskList update={this.state.update}/>
                    </GridColumn>
                    <GridColumn>
                        <ProcessingUnit updateList={(notification) => this.update(notification)}/>
                    </GridColumn>
                    <GridColumn>
                        <CompletedTaskList update={this.state.update}/>
                    </GridColumn>
                </Grid>
            </Segment>
            {/*<IfBox shouldShow={this.state.showInstructions}>*/}
            {/*    <Instructions onClose={() => {this.setState({showInstructions: false})}} use_bot={!this.state.showFieldOnly}/>*/}
            {/*</IfBox>*/}
        </Container>
    }

    private update(notification: Notification) {
        this.setState((prevState) => ({
            update: !prevState.update,
            hasError: notification?.hasError,
            showNotification: notification?.showNotification,
            notification: notification?.notification,
            startTime: service.timeElapsed
        }), () => setTimeout(() => this.setState({showNotification: false}), 2000));
    }
}