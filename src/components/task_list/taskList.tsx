import * as React from "react";
import {
    Button,
    Container, Divider, Grid,
    Header,
    Segment, Table
} from "semantic-ui-react";
import {Task, service} from "../../Service";
import {v4 as uuid} from 'uuid';


export interface TaskListProps {
    update: boolean,
}

export interface TaskListState {
    loading: boolean
    update: boolean
    tasks: Task[]
}


export class TaskList extends React.Component<TaskListProps, TaskListState> {

    constructor(props: any) {
        super(props)
        this.state = {
            loading: false,
            update: false,
            tasks: service.tasks
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps: Readonly<TaskListProps>, prevState: Readonly<TaskListState>, snapshot?: any) {
        if (this.state.update !== this.props.update) {
            // console.log("LIST STATE",this.state)
            this.setState({update: this.props.update, tasks: service.tasks})
            this.forceUpdate()
        }
    }

    render() {
        return <Container>
            <Header as='h5' floated='right'>
                <Button type="submit" onClick={this.deleteAllTasks}>Delete All</Button>

            </Header>
            <Header as='h2' floated='left'>
                Queued Tasks
            </Header>
            <Divider hidden/>
            <Divider hidden/>
            <Divider hidden/>
            <Divider />
            <Segment attached style={{padding: "10px"}} loading={this.state.loading}>
                <Table basic='very' celled collapsing style={{overflow: "hidden", width: "100%"}}>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Task</Table.HeaderCell>
                            <Table.HeaderCell>Start Time</Table.HeaderCell>
                            <Table.HeaderCell>Exec Time</Table.HeaderCell>
                            {(service.algo === 'Priority') ? <Table.HeaderCell>Priority</Table.HeaderCell> : null}
                            <Table.HeaderCell>Actions</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.tasks.map((task, i) =>
                            <Table.Row key={uuid()}>
                                <Table.Cell>{task.name}</Table.Cell>
                                <Table.Cell>{task.startTime}</Table.Cell>
                                <Table.Cell>{task.executionTime}</Table.Cell>
                                {(service.algo === 'Priority') ? <Table.Cell>{task.priority}</Table.Cell> : null}
                                <Table.Cell>
                                    <Button onClick={() => this.removeTask(task.id)} size='mini' color={"red"} circular
                                            icon='delete'/>
                                </Table.Cell>

                            </Table.Row>)}
                    </Table.Body>
                </Table>
            </Segment>
        </Container>
    }

    removeTask = (id) => {
        service.removeTask(id)
        this.setState({tasks: service.tasks});
        // console.log(service.tasks)
    }
    deleteAllTasks = () => {
        service.tasks = []
        this.setState({tasks: service.tasks});
        // console.log(service.tasks)
    }

}