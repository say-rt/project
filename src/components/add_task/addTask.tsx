import * as React from "react";
import {
    Button, Checkbox,
    Container, Divider,
    Form,
    FormField,
    Grid,
    Header,
    Input,
    Segment
} from "semantic-ui-react";
import {Task, service} from "../../Service";
import {IfBox} from "../style/if";
import {v4 as uuid} from 'uuid';
import {TaskListProps, TaskListState} from "../task_list/taskList";

export interface AddTaskProps {
    startTime: number,
    taskAdded: (notification) => void,
}

export interface AddTaskState {
    name: string,
    startTime: number,
    executionTime: number,
    recurring: boolean
    recurringInterval: number
    priority: number
    loading: boolean
}


export class AddTask extends React.Component<AddTaskProps, AddTaskState> {
    resetBtn: any;

    constructor(props: any) {
        super(props)
        this.state = {
            name: 'T' + service.totalTasks,
            startTime: service.timeElapsed > 0 ? service.timeElapsed : 0,
            executionTime: 1,
            priority: 0,
            recurring: false,
            recurringInterval: 1,
            loading: false,
        }
    }

    componentDidMount() {
    }

    componentDidUpdate(prevProps: Readonly<AddTaskProps>, prevState: Readonly<AddTaskState>, snapshot?: any) {
        if(service.processing && this.props.startTime && this.state.startTime < this.props.startTime){
            // console.log("LIST STATE",this.state)
            this.setState({startTime: this.props.startTime})
            this.forceUpdate()
        }
    }

    onNameChange = (e) => {
        this.setState({name: e.target.value})
    };
    onStartTimeChange = (e) => {
        if(+e.target.value > +service.timeElapsed){
            this.setState({startTime: e.target.value})
        }
    };
    onExecutionTimeChange = (e) => {
        this.setState({executionTime: e.target.value})
    };
    onRecurringChange = (e) => {
        this.setState((prevState) => ({recurring: !prevState.recurring}))
    };
    onRecurringIntervalChange = (e) => {
        this.setState({recurringInterval: e.target.value})
    };
    onPriorityChange = (e) => {
        console.log(e.target.value)
        this.setState({priority: e.target.value})
    };

    render() {
        return <Container>

            <Header as='h2' floated='left'>
                Add Tasks
            </Header>
            <Divider hidden/>
            <Divider hidden/>
            <Divider hidden/>
            <Divider />
            <Segment attached>
                <Form>
                    <Grid>
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Name:</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                <FormField>
                                    <Input placeholder={this.state.name} loading={this.state.loading} error={false}
                                           onChange={this.onNameChange}/>
                                </FormField>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Start Time:</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                <FormField>
                                    <Input placeholder={this.state.startTime + " ms"} loading={this.state.loading}
                                           error={false} type="number"
                                           min={service.timeElapsed || 0} onChange={this.onStartTimeChange}/>
                                </FormField>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Execution Time:</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                <FormField>
                                    <Input placeholder={this.state.executionTime + " ms"} loading={this.state.loading}
                                           error={this.state.executionTime < 1} type="number"
                                           min={1} onChange={this.onExecutionTimeChange}/>
                                </FormField>
                            </Grid.Column>
                        </Grid.Row>
                        <IfBox shouldShow={service.algo === 'Priority'}>
                            <Grid.Row columns={2}>
                                <Grid.Column width={6} verticalAlign={"middle"}>
                                    <label>Priority:</label>
                                </Grid.Column>
                                <Grid.Column textAlign={"right"}>
                                    <FormField>
                                        <Input placeholder={this.state.priority}
                                               loading={this.state.loading} type="number"
                                               min={1} onChange={this.onPriorityChange}/>
                                    </FormField>
                                </Grid.Column>
                            </Grid.Row>
                        </IfBox>
                        <Grid.Row columns={2}>
                            <Grid.Column width={6} verticalAlign={"middle"}>
                                <label>Recurring</label>
                            </Grid.Column>
                            <Grid.Column textAlign={"right"}>
                                <FormField>
                                    <Checkbox toggle checked={this.state.recurring} onChange={this.onRecurringChange}/>
                                </FormField>
                            </Grid.Column>
                        </Grid.Row>
                        <IfBox shouldShow={this.state.recurring}>
                            <Grid.Row columns={2}>
                                <Grid.Column width={6} verticalAlign={"middle"}>
                                    <label>Recurring Interval:</label>
                                </Grid.Column>
                                <Grid.Column textAlign={"right"}>
                                    <FormField>
                                        <Input placeholder={this.state.recurringInterval + " ms"}
                                               loading={this.state.loading}
                                               error={this.state.recurringInterval < 1} type="number"
                                               min={1} onChange={this.onRecurringIntervalChange}/>
                                    </FormField>
                                </Grid.Column>
                            </Grid.Row>
                        </IfBox>
                        <Grid.Row>
                            <Grid.Column textAlign={"right"}>
                                <Button type="submit" onClick={this.AddTask}>Add</Button>
                                <Button onClick={this.resetFields} type="reset">Reset Fields</Button>
                                <input hidden ref={btn => this.resetBtn = btn} type="reset" value="Reset"/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>

                </Form>
            </Segment>
        </Container>
    }

    resetFields = () => {
        this.setState({
            name: 'T' + service.totalTasks,
            startTime: service.timeElapsed > 0 ? service.timeElapsed : 0,
            executionTime: 1,
            priority: 0,
            recurring: false,
            recurringInterval: 1
        })
        this.resetBtn.click()
    }

    AddTask = () => {
        const notification = {
            hasError: this.state.executionTime < 1,
            showNotification: true,
            notification: this.state.executionTime < 1 ? "Execution Time must be at least 1ms" : "Task successfully added."
        }
        //todo: startTime < timeElasped check is not working
        if (this.state.executionTime >= 1) {
            let newTask = new Task();
            newTask.initialize(this.state.name, this.state.startTime, this.state.executionTime, this.state.recurring, this.state.recurringInterval, this.state.priority);
            if (newTask.recurring) {
                service.addTask(newTask)
                const newInterval = setInterval((task) => {
                    console.log(uuid());
                    let newRecurringTask2 = new Task();
                    newRecurringTask2.name = newTask.name + "(r)"
                    newRecurringTask2.startTime = newTask.startTime > service.timeElapsed ? newTask.startTime : service.timeElapsed;
                    newRecurringTask2.executionTime = newTask.executionTime;
                    newRecurringTask2.priority = newTask.priority;
                    console.log(service.tasks)
                    service.addTask(newRecurringTask2)
                    this.props.taskAdded(notification)
                }, newTask.recurringInterval * 1000)
                service.recurringTimers.push(newInterval);
            } else {
                service.addTask(newTask)
            }
            this.resetFields();
        }
        this.props.taskAdded(notification);

    }
}