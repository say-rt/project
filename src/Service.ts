import {v4 as uuid} from 'uuid';

export type Notification = {
    hasError: boolean,
    showNotification: boolean,
    notification: string
}
//     First-Come, First-Served (FCFS) Scheduling.
//     Shortest-Job-Next (SJN) Scheduling.
//     Priority Scheduling.
//     Shortest Remaining Time.
//     Round Robin(RR) Scheduling.
//     Multiple-Level Queues Scheduling.
export type Algo = 'FCFS' | 'SJN' | 'Priority'

export class Task {
    id: string
    name: string
    startTime: number
    executionTime: number
    completionTime?: number
    elapsedTimeOnMe: number
    waitTime: number
    priority: number
    inProcessing: boolean
    recurring: boolean
    recurringInterval: number
    processingStartedOn: number
    recurringTimer: any

    constructor() {
        this.id = uuid();
        this.name = 'P' + service.totalTasks;
        this.startTime = 0;
        this.executionTime = 0;
        this.elapsedTimeOnMe = 0;
        this.processingStartedOn = service.timeElapsed;
        this.completionTime = null;
        this.inProcessing = false;
        this.recurring = false;
        this.recurringInterval = 1;
        this.recurringTimer = null;
    }

    initialize = (name: string, startTime: number, executionTime: number, recurring: boolean, recurringInterval: number,priority: number) => {
        this.id = uuid();
        this.name = name !== "" ? name : this.name;
        if (service.processing) {
            this.startTime = startTime < service.timeElapsed ? service.timeElapsed : startTime;
        } else {
            this.startTime = startTime;
        }
        this.executionTime = executionTime;
        this.priority = priority;
        this.recurring = recurring;
        this.recurringInterval = recurringInterval;
    }

    complete = (time) => {
        this.completionTime = time
        this.waitTime = time - this.executionTime;
        service.moveToCompleted(this)
        this.inProcessing = false;
    }
    start = (time) => {
        this.inProcessing = true;
        this.processingStartedOn = time;
    }
    process = () => {
        if (this.startTime <= service.timeElapsed) {
            this.elapsedTimeOnMe++
        }
    }

}

class Service {
    algos: any[] = [
        {
            text: "First-Come, First-Served",
            value: "FCFS",
            key: "fcfs"
        },
        {
            text: "Shortest-Job-Next (SJN) Scheduling",
            value: "SJN",
            key: "sjn"
        },
        {
            text: "Priority Scheduling",
            value: "Priority",
            key: "priority"
        }
        // {
        //     text: "     Shortest Remaining Time",
        //     value: "srt",
        //     key: "srt"
        // },
        // {
        //     text: "     Round Robin(RR) Scheduling",
        //     value: "rr",
        //     key: "rr"
        // }
    ];
    algo: Algo = 'FCFS';
    timeElapsed: number = 0;
    processing: boolean = false;
    finished: boolean = false;
    stoppedOnce: boolean = false;
    stopOnFinish: boolean = false;
    totalTasks: number = 0;
    tasks: Task[] = [];
    recurringTimers: any[] = [];
    completedTasks: Task[] = [];

    addTask = (newTask) => {
        service.tasks.push(newTask);
        this.totalTasks++;
    }
    removeTask = (id) => {
        service.tasks = service.tasks.filter(task => task.id !== id);
    }

    moveToCompleted = (task) => {
        this.completedTasks.push(task)
    }
    algoSelection = () => {
        let task;
        switch (this.algo) {
            case "FCFS":
                task = this.FCFS()
                break;
            case "SJN":
                task = this.SJN()
                break;
            case "Priority":
                task = this.Priority()
                break;

        }
        return task;
    }
    FCFS = () => {
        let selectedTask = service.tasks[0];
        service.tasks.forEach(task => {
            if (task.startTime < selectedTask.startTime) {
                selectedTask = task
            }
        });
        return selectedTask;
    }
    SJN = () => {
        let selectedTask = service.tasks[0];
        service.tasks.forEach(task => {
            if (task.executionTime < selectedTask.executionTime) {
                selectedTask = task
            }
        });
        return selectedTask;
    }
    Priority = () => {
        let selectedTask = service.tasks[0];
        service.tasks.forEach(task => {
            if (+task.priority > +selectedTask.priority) {
                selectedTask = task
            }
        });
        return selectedTask;
    }
}

export const service = new Service()