import React from "react";
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
// import {service} from "./service";
// import {Consent} from "./components/Consent/Consent";
import {Home} from "./components/Home/Home";

//
// const RouteTo = ({ component, perms}: any) => {
//     const routeComponent = (props: any) => (
//         service.consent ? (<Redirect to="/search"/>) : (<Redirect to="/consent"/>)
//     );
//     return <Route render={routeComponent} />;
// };


export default function AppRouter() {
    return (
        <Router>
            <div>
                {window.location.pathname.includes('index.html') && <Redirect to="/"/>}
                <Switch>
                    <Route path="/" component={Home} />
                    {/*<RouteTo exact path="/" render={() => (service.consent ? (<Redirect to="/search"/>) : (<Redirect to="/consent"/>))}/>*/}
                    {/*<Route path="/search" component={Home} />*/}
                    {/*<Route path="/consent" component={Consent} />*/}
                    {/*<RouteTo></RouteTo>*/}
                </Switch>
            </div>
        </Router>
    );
}
